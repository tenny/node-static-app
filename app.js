const express = require('express')
const app = express()
const port = 3002

// static files
app.use(express.static('public') )
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/img', express.static(__dirname + 'public/img'))
app.use('/js', express.static(__dirname + 'public/js'))

// Set Views
app.set('views', './views')
app.set('view engine', 'ejs')


// endpoints
app.get('/', (req,res) => {
    res.render('index', { text: 'Hello, displaying Homepage v1 '} )
})

app.get('/about', (req,res) => {
    res.render('about', { text: 'Hello, displaying About Page'} )
})

app.listen(port,() => {
    console.log(`app is running on port ${port}`)
} )